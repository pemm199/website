/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    $('#ecc-read-more-trigger').click(function() {
        // animate container height (css in callback is to eliminate wonkiness after animation)
        var $container = $('#ecc-description-container');
        $container.animate(
            {height: $container.get(0).scrollHeight}, 
            500,
            function() { $container.css({height: 'auto'}); }
        );
        // remove the button
        $(this).parent().remove();
    });

    loadAnimation('#multi-chain-animation', '/assets/js/animation/multi_chain.json');
    loadAnimation('#file-storage-animation', '/assets/js/animation/file_storage.json');
    loadAnimation('#messaging-animation', '/assets/js/animation/messaging.json');
    loadAnimation('#name-resolution-animation', '/assets/js/animation/name_resolution.json');

    /*
     * Timeline
     */
    var toggleTimelineButtons = function(
        $timelineEvents, 
        timelineEventWidth, 
        $timelineScroller, 
        $timelinePrevious, 
        $timelineWrapper, 
        $timelineNext
    ) {
        // We're not set to the beginning--enable the button
        if ($timelineScroller.scrollLeft() != 0) {
            $timelinePrevious.removeClass('disabled');
        } else {
            $timelinePrevious.addClass('disabled');
        }

        // The last event isn't visible (use have the width to not require too much to the right of the last item)
        var lastEventLeft = $timelineEvents.eq($timelineEvents.length - 1).offset().left - $timelineWrapper.offset().left;
        if ((lastEventLeft + (timelineEventWidth / 2)) >= ($timelineScroller.scrollLeft() + $timelineScroller.width())) {
            $timelineNext.removeClass('disabled');
        } else {
            $timelineNext.addClass('disabled');
        }
    }
    var $timelineEvents = $('.timeline-event');
    var $timelineScroller = $('#timeline-scroller');
    var $timelinePrevious = $('.timeline-previous');
    var $timelineWrapper = $('.timeline-line-wrapper');
    var $timelineNext = $('.timeline-next');
    var timelineEventWidth = $timelineEvents.eq(1).outerWidth(true);
    $timelineWrapper.css({width: ($timelineEvents.length * timelineEventWidth) + 'px'});
    $timelineEvents.click(function() {
        var $timelineEvent = $(this);
        var timelineEventLeft = $timelineEvent.offset().left - $timelineWrapper.offset().left;
        $('.timeline-line').css({width: timelineEventLeft + 'px'});
        toggleTimelineButtons($timelineEvents, timelineEventWidth, $timelineScroller, $timelinePrevious, $timelineWrapper, $timelineNext);
        $('#timeline-event-detail-display').html($timelineEvent.find('.timeline-event-detail').html());
    });
    $timelinePrevious.click(function() {
        $timelineScroller.animate(
            {scrollLeft: $timelineScroller.scrollLeft() - $timelineScroller.width()}, 
            250,
            function() {
                toggleTimelineButtons($timelineEvents, timelineEventWidth, $timelineScroller, $timelinePrevious, $timelineWrapper, $timelineNext);
            }
        );
    });
    $timelineNext.click(function() {
        $timelineScroller.animate(
            {scrollLeft: $timelineScroller.scrollLeft() + $timelineScroller.width()}, 
            250,
            function() {
                toggleTimelineButtons($timelineEvents, timelineEventWidth, $timelineScroller, $timelinePrevious, $timelineWrapper, $timelineNext);
            }
        );
    });
    $timelineEvents.eq(2).click();
});