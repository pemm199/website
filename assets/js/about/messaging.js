/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#privacy-animation', '/assets/js/about/messaging/animation/privacy.json');
    loadAnimation('#instant-messaging-animation', '/assets/js/about/messaging/animation/instant_messaging.json');
    loadAnimation('#file-transfer-animation', '/assets/js/about/messaging/animation/file_transfer.json');
    loadAnimation('#uptime-animation', '/assets/js/about/messaging/animation/uptime.json');
});