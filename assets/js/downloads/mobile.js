/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#mobile-currency-animation', '/assets/js/downloads/mobile/animation/currency.json');
    loadAnimation('#mobile-messaging-animation', '/assets/js/downloads/mobile/animation/messaging.json');
    loadAnimation('#mobile-file-storage-animation', '/assets/js/downloads/mobile/animation/file_storage.json');
});