/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

function hideModal() {
    $('body').off('keyup', escapeKeyHandler);
    $('#modal-wrapper').hide()
        .off('click', modalClickHandler);
}

function escapeKeyHandler(e) {
    if (e.which == 27) {
        hideModal();
    }
}

function modalClickHandler(e) {
    if (!$(e.target).hasClass('img')) {
        hideModal();
    }
}

$(function() {
    $('.sapphire-feature .img').click(function() {
        var $newImg = $(this).clone();
        $newImg.css({width: '100%'})
            .find('.mobile-src')
            .remove();
        $('#modal-content').html($newImg);
        $('#modal-wrapper').show()
            .on('click', modalClickHandler);
        $('body').on('keyup', escapeKeyHandler);
    });
});